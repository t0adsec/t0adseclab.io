# ToadSec Article Archive 🎨

> [Sitio de Referencia para Usar el Tema](https://sylhare.github.io/Type-on-Strap/)


## Structure

Here are the main files of the template

```bash
Type-on-Strap
├── _includes	              
├── _layouts                   
├── _portfolio	               
├── _posts                     # METAN LOS ARCHIVOS DE SUS POSTS AQUI
├── _sass                      
├── assets
|  ├── js	              
|  ├── css                     
|  ├── fonts		       
|  └── img		      
|   ├── posts         # Metan las fotos de su post en esta carpeta!!!!!!
├── pages
|   ├── 404.md		       
|   ├── about.md               
|   ├── gallery.md             
|   ├── portfolio.md	       
|   ├── search.html	       
|   └── tags.md               
├── _config.yml                
├── _data.yml
|  ├── authors.yml             
|  ├── language.yml            
|  └── social.yml              
└── index.html                
```


#### Encabezado

Todos los posts tienen que tener minimo el siguiente encabezado:

```yml
---
layout: post
title: Hello World
---
```

Pero estos son todos los posibles encabezados utilizables. En la documentacion del tema sale que hacen.
```yml

---
layout: post
title: Hello World                                # Title of the page
hide_title: true                                  # Hide the title when displaying the post, but shown in lists of posts
feature-img: "assets/img/sample.png"              # Add a feature-image to the post
thumbnail: "assets/img/thumbnails/sample-th.png"  # Add a thumbnail image on blog view
color: rgb(80,140,22)                             # Add the specified color as feature image, and change link colors in post
bootstrap: true                                   # Add bootstrap to the page
tags: [sample, markdown, html]
---
```

With `thumbnail`, you can add a smaller image than the `feature-img`. 
If you don't have a thumbnail you can still use the same image as the feature one. Or use the gulp task to create it.

The background used when `color` is set comes from `lineart.png` from [xukimseven](https://github.com/xukimseven) 
you can edit it in the config file (`_config.yml > color_image`). If you want another one, put it in `/assets/img` as well. 

The **bootstrap** is not mandatory and is only useful if you want to add bootstrapped content in your page. 
It will respect the page and theme layout, mind the padding on the sides.

#### Post excerpt

The [excerpt](https://jekyllrb.com/docs/posts/#post-excerpts) are the first lines of an article that is display on the blog page. 
The length of the excerpt has a default of around `250` characters or can be manually set in the post using:

in `conflig.yml`:

```yml
excerpt: true
```

Then in your post, add the `excerpt separator`:

```yml

---
layout: post
title: Sample Page
excerpt_separator: <!--more-->
---

some text in the excerpt
<!--more-->
... rest of the text not shown in the excerpt ...
```

The html is stripped out of the excerpt, so it only displays text.

#### Image aligner

To easily add align images side by side in your article using the `aligner.html` include:

```ruby
{% include aligner.html images="path/to/img1.png,path/to/img2.png,path/to/img3.png" column=3 %}
```

Use it in any markdown file. There are two fields in the _include_ you need to look into:
  - _images_: Takes a string separated with `,` of all the images' path. 
    - It by default look into `assets/img/` so give the path from there.
  - _column_: (OPTIONAL) Set the number of column you want your imaged displayed in.
    - default is 2 columns
    - `column=3` set 3 columns
    - `column="auto"` makes as many columns as images

#### Code highlight

Like all CSS variables in the theme, you can edit the color of the code highlight in *_sass > base > _variables.scss*.
The code highlighting works with [base16](https://github.com/chriskempson/base16-html-previews/tree/master/css) you can find existing example 
of your favourite highlight color scheme on this format.

## Feature pages and layouts 

All feature pages besides the "home" one are stored in the `page` folder, 
they will appear in the navigation bar unless you set `Hide: true` in the front matter. 

Here are the documentation for the other feature pages that can be added through `_config.yml`. 

Non-standard features are documented below.

### Layout: Default

This layout includes the head, navigation bar and footer around your content. 
Unless you're making a custom layout you won't need it.

### Layout: Home 🏡

This page is used as the home page of the template (in the `index.html`). It displays the list of articles in `_posts`.
You can use this layout in another page (adding a title to it will make it appear in the navigation bar).

The recommended width and height for the home picture is width:`2484px;` and height:`1280px` 
which are the dimensions of the actual picture for it to be rolling down as you scroll the page.

If your posts are not displaying ensure that you have added the line `paginate: 5` to `_config.yml`.

### Layout: Page 📄

The page layout have a bit more features explained here.

```yml

---
layout: page
title: "About" 
subtitle: "This is a subtitle"   
feature-img: "assets/img/sample.png" 
permalink: /about.html               # Set a permalink your your page
hide: true                           # Prevent the page title to appear in the navbar
icon: "fa-search"                    # Will Display only the fontawesome icon (here: fa-search) and not the title
tags: [sample, markdown, html]
---
```

The hide only hides your page from the navigation bar, it is however still generated and can be access through its link. 

