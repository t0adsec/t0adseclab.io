---
layout: page
title: About
permalink: /about/
feature-img: "assets/img/pexels/travel.jpeg"
tags: [About, Archive]
---

Toad Security se dedica a la investigación en materia de seguridad informática, con el objetivo de asesorar empresas en el tema, fortaleciendo su infraestructura informática en muchos frentes.
 
Tenemos la experiencia, certificaciones, y hemos participado en múltiples conferencias de marcas locales e internacionales, compartiendo nuestro conocimiento tanto en ponencias, como en talleres.

La idea de este pequeño espacio en internet, es devolver un poco de lo que la comunidad nos ha regalado en este tiempo que llevamos estudiando. Esperemos que puedan aprender un poco, y que el contenido sea del agrado de todos.