---
layout: post
title: "Hack The Planet 1: From Zero to Site Admin, con un POST request y haciendome el loco"
author: tiz
feature-img: "assets/img/posts/htp-geekyd/banner.png"
thumbnail: "assets/img/posts/htp-geekyd/banner.png"
---

La idea de la serie de posts "Hack The Planet" es buscar vulnerabilidades en casos de estudio del mundo real, en aplicaciones que estén funcionando ya mismo, reportarlas a quien corresponda antes de que puedan ser abusadas, explicarlas para sacar un valor investigativo que pueda ser compartido con los lectores de nuestro blog, y regalar una mirada a como se ve el hacking en la vida real, con el objetivo de educar un poco sobre lo que hacemos en Toad Security.

## Disclaimer

Todas las pruebas realizadas, fueron hechas con fines educativos. No es nuestra intención en ningún caso menospreciar el trabajo de los desarrolladores del equipo de Geeky Drop, ni de nadie. Se aprecia y se admira el producto local, y por esa razón es que contactamos al CEO de GeekyDrop para comunicarle de este tema con muchas semanas de anticipación, para solicitar permiso de hacer esta publicación.

Al día de hoy, 21 de mayo de 2021, el exploit ha sido solucionado, pero esto no significa que no hayan mas cosas ;) 

## Contexto

En esta primera entrega, el objeto de estudio fue la comunidad de ventas en linea de Panamá, GeekyDrop. GeekyDrop es, sin lugar a dudas, la plataforma de compraventa más importante del momento a nivel nacional en Panamá. Para nuestros lectores extranjeros, GeekyDrop es una comunidad de comercio electrónico al puro estilo de ebay, en la cual sus usuarios pueden comprar y vender productos sin mayor problema, con un sistema super sencillo de verificación de transacciones. 

Como buen cibernauta, le eché un ojo para ver que ofrecía, y para mi sorpresa, se consiguen cosas muy interesantes a buenos precios. Pero, ¿Qué tan asegurada está Geeky Drop contra un ataque cibernético?

La idea era intentar explorar los mecanismos que hacían que GeekyDrop funcionara, encontrar algún API expuesto que fuera medio liberal, alguna función secreta que no estuviera a simple vista, o cualquier otra peculiaridad que me permitiera manipular el sitio de alguna forma _curiosa_.

## Registrando una cuenta con cizaña

El primer paso para interactuar con las secciones más interesantes de GeekyDrop es registrar una cuenta. Sin embargo, ¿Como hago para registrarme en una plataforma que requiere tanta verificación para habilitar tu cuenta? Aquí es donde encontramos nuestra primera falla de diseño, en el proceso de registro. 

GeekyDrop maneja las verificaciones de sus usuarios de manera manual, y dicho proceso se hace luego de que un miembro del staff haya validado de que el usuario en efecto cumple con las especificaciones que solicitan, al momento de registrar las cuentas en la plataforma. Estos requisitos incluyen una fotografía de un documento personal como cédula o pasaporte, una fotografía actual de la persona sosteniendo dicho documento, y eventualmente para hacer transacciones se solicita una foto con los cuatro últimos dígitos de una tarjeta de crédito, lo cual es bastante inofensivo por sí solo. 

{% include aligner.html images="posts/htp-geekyd/1.png,posts/htp-geekyd/2.png" %}

Sin embargo, el problema está justamente en el proceso. En el momento que un usuario se registra, automáticamente se le crea un token de autenticación. Este token ya le permite interactuar con partes críticas del sitio, incluso antes de completar el registro. Esta falla de diseño es crítica, puesto que es la que habilita que la siguiente falla aumente en criticidad. 

![3](/assets/img/posts/htp-geekyd/3.png)

Como bonus, es posible bypassear el CAPTCHA del proceso de registro, los cual podría habilitar una denegación de servicio al llenar violentamente el almacenamiento de imágenes de gatos, ser utilizado como almacenamiento de payloads peligrosos en el bucket de Digital Ocean Spaces que usan.


## ¡Hey niño! Cambiame este POST Request ahí en el backend porfa, y me traes mi vuelto

El siguiente objetivo al tener una cuenta dentro del sitio, era encontrar alguna manera de saltarse el proceso de verificación. Mi teoría era ver si se mantenían estos parámetros de verificación del lado del cliente, y en algún momento se agregan a un whitelist dentro de la base de datos del sitio, para completar el proceso de validación. Esta teoría no rindió muchos frutos, puesto que a pesar de que si se almacena el usuario dentro del token de autenticación en el header, este no puede ser modificado tan fácilmente sin la segunda parte de la llave asimétrica para firmar los JWT (JSON Web Tokens).

![4](/assets/img/posts/htp-geekyd/4.png)

![5](/assets/img/posts/htp-geekyd/5.png)

La siguiente teoría era ver si podía manipular el objeto de usuario dentro de la base de datos, desde el API. Un API (en el entorno web) es un conjunto de herramientas que permiten interactuar programáticamente con los sistemas de una aplicación, sitio web o infraestructura. Se tiende a implementar un API cuando se desea mantener la modularidad en el diseño del proyecto, y que sea fácil extender sus funcionalidades en el futuro [0]. 

Usualmente se implementan funciones para hacer queries al motor de la base de datos, como es el caso de las categorías de los productos. Estos se le piden al API, y el retorna el listado de categorías, para que sea renderizado por el frontend.

![6](/assets/img/posts/htp-geekyd/6.png)

Analizando las distintas llamadas al API que se hacían en las distintas secciones del Sitio, encontramos que un EndPoint [1] modifica parámetros del usuario como su nombre, su número de cédula, su foto de perfil, entre otros. Esta es una función potencialmente peligrosa, que incluso está contemplada dentro del [OWASP API Top 10](https://owasp.org/www-project-api-security/) [2], específicamente en el puesto número 6.

![7](/assets/img/posts/htp-geekyd/7.png)

"Mass Assignment", según OWASP Foundation, ocurre cuando se mantienen funciones de manipulación de objetos del lado del cliente, que afectan la base de datos sin ningún tipo validación o  de filtrado de propiedades en algún mecanismo de WhiteList, o algo que detenga a cualquier persona de _simplemente pedirle por favor al API que le cambie de un valor a otro_. En este caso, existe un control parcial, puesto que no es posible interactuar con este endpoint sin tener un token de autenticación válido, pero como vimos en la sección pasada, es trivial conseguir uno válido al registrarnos.

Como prueba de concepto, intentemos modificar el Nombre y el Apellido de nuestro usuario de prueba directamente desde el API, sin lidiar con los controles de la vista del sitio.

![8](/assets/img/posts/htp-geekyd/8.png)

Antes

![9](/assets/img/posts/htp-geekyd/9.png)

Despues

En medio de la redacción de este post, me borraron mi cuenta de pruebas, pero en la foto se aprecia como funcionaria la edición de los parámetros sin mucho problema. 

## El agua hace flotar el barco, pero también puede hundirlo 

¿Que podría hacer un adversario malicioso con algo como esto? La manipulación del objeto no solamente permite la modificación de atributos como el nombre o el apellido. Un objeto de usuario en la base de datos de cualquier aplicación, usualmente maneja cosas como la verificación de la cuenta, si es un usuario con privilegios elevados, si este usuario está baneado, y muchos otros parámetros internos para el funcionamiento correcto de la aplicación.

La tercera falla que se encontró fue que el API expone demasiada información del objeto del usuario, lo cual me permite saber exactamente qué es lo que debo modificar para obtener los resultados que quiero. Es decir, Validé mi propia cuenta como un usuario legítimo, me volví administrador y por las risas me puse de Score de Usuario 420.

![10](/assets/img/posts/htp-geekyd/10.png)

![11](/assets/img/posts/htp-geekyd/11.png)


Con estas modificaciones, quede con un usuario como este: 

![12](/assets/img/posts/htp-geekyd/12.jpg)

Ya con este nivel, tendría un mundo de posibilidades dentro de la aplicación, pero esto es suficiente. Se procedió con un reporte informal por Instagram con el CEO de GeekyDrop, el cual lo tomó muy abiertamente, conversamos un rato, y le solicité que me permitiera hacer este post.

Hay que resaltar que esta función de modificación de objetos no es inherentemente mala o insegura. Lo que pasa es que por diseño, no filtra los atributos modificables, lo cual la hace propensa a ser abusada de esta manera.

En conclusión, quedamos con una kill chain como esta:


Account Validation Bypass > Object Manipulation via Insecure Mass Assignment > Priv Esc to Site Admin via Object Manipulation


### Timeline

- 25/04/2021 - Primer Contacto con GeekyDrop por Instagram para hacer el reporte
- 02/05/2021 - Segundo contacto, confirmando si se habia realizado el triage correspondiente
- 20/05/2021 - Reintentando la explotacion, y se encontro que se habia parcheado parte del problema utilizando alguna clase de Filtro
- 21/05/2021 - Redacción, publicacion y divulgacion responsable de la vulnerabilidad

### Referencias y Notas


- [0] Claro, un API implica mucho mas que solo esto, pero me permitire sintetizar la explicacion de un API para facilitar la comprension en este contexto.
- [1] Una de las funciones del API
- [2] El top 10 de Vulnerabilidades de APIs compilado por la OWASP Foundation, la fundacion sin fines de lucro de facto en temas de ciberseguridad. https://owasp.org/www-project-api-security/