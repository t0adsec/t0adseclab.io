---
layout: post
title: "HackTheBox - OpenSource - PrivEsc Using Git Hooks" 
author: nahjose06
excerpt_separator: <!--more-->
---

El propósito de este post es mostrar lo vulnerable y peligroso que puede ser tener una mala gestión de los permisos de cualquier software. En esta ocasión, trabajaremos con Git.

Git es utilizado por muchísimas personas y empresas alrededor del mundo, por lo que es muy importante asegurarnos de saber cómo prevenir que pueda ser explotado por usuarios malintencionados.

Si no sabes qué es Git, básicamente es un software que sirve básicamente para gestionar las versiones por las que va pasando el código de los proyectos, en pocas palabras, Git permite mantener un control sobre los cambios en el proyecto.

<!--more-->

Después de saber qué es Git, vamos a la parte divertida.



**Laboratorio**

Para practicar la explotación de esta vulnerabilidad, utilizaremos la máquina OpenSource, de la plataforma [HackTheBox](https://www.hackthebox.com/), una máquina Linux de dificultad fácil, pero por la cantidad de pasos y explotaciones yo la calificaría como media.

![](//assets/img/posts/PrivEsc-Using-Git-Hooks/1.png)



**Acceso Inicial**

Iniciaremos esta máquina haciendo un escaneo con **Nmap**, donde conseguimos los puertos 22 y 80 abiertos, pero el puerto 3000 filtrado.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/2.png)

Si abrimos con el navegador el puerto 80, vemos una aplicación web donde podemos subir archivos a un servidor. Gracias a **Wappalizer** podemos ver que esta aplicación ha sido escrita en Python, utilizando el famoso framework Flask.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/3.png)

En la parte inferior del sitio, vemos que podemos descargar el código fuente de la aplicación, si lo descargamos podemos ver una carpeta **.git**.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/4.png)


Después de analizar el archivo, encontramos que en una versión anterior había credenciales en texto claro en un archivo. Pero estas credenciales no nos sirven para utilizar SSH.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/5.png)

Regresando al sitio web, vemos en la parte inferior una opción que nos llevará a la aplicación en producción.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/6.png)



Decidimos subir una imagen para probar y descubrimos que los archivos se almacenan en una carpeta llamada **uploads**, similar a una carpeta encontrada en el código fuente de la aplicación.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/7.png)




Al ver esto, se nos ocurrió intentar un Path Traversal para reemplazar el archivo **views.py** para poder tener una ejecución remota de comandos **(RCE)**, por lo que editamos el archivo **views.py**. A la hora de subirlo, interceptamos la petición y cambiamos el destino para que sea la localización del archivo **views.py** original.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/8.png)

Luego de hacer esto, pudimos conseguir un RCE, por lo que fácilmente podemos conseguir una reverse shell.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/9.png)

Una vez en la máquina, encontramos que no estamos en la máquina, sino que estamos en una especie de contenedor. Por lo que debemos escapar del mismo para obtener acceso a la máquina.

Si recordamos, en el escaneo de puertos encontramos que el puerto 3000 estaba filtrado, por lo que intentamos traerlo a nuestra máquina utilizando nuestro queridísimo amigo Chisel.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/10.png)

Al abrir el puerto 3000 en nuestro navegador, encontramos un Gitea.

![](11.png)

Aquí intentamos utilizar las credenciales que ya habíamos encontrado en la carpeta **.git**, y tuvimos acceso.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/12.png)

Después de un rato buscando algo interesante en este repositorio, encontramos un archivo **id\_rsa**.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/13.png)

Así que de esta manera pudimos conseguir shell en el servidor base, es decir nuestro objetivo.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/14.png)


**Escalado de Privilegios**

Ahora sí vamos con todo a lo que nos compete en esta entrada, el PrivEsc usando Git Hooks.

Si ejecutamos Pspy, encontramos que Root ejecuta una serie de tareas con Git cada minuto.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/15.png)

Aquí encontramos dos cosas interesantes, primero que se ejecuta con Bash el script ***/usr/local/bin/git-sync.*** 

Después de abrir el archivo, para analizarlo, vemos que hace un *commit* en la carpeta del usuario dev01 (el usuario que tenemos).

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/16.png)

Esto significa que cada minuto Root ejecuta este script para realizar un *commit* de la carpeta base del usuario **dev01**. Por ende, podríamos revisar la carpeta **.git**, para ver permisos.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/17.png)

Aquí encontramos el primer error, el usuario **dev01** es dueño de la carpeta **.git**, por lo que puede hacer todos los cambios deseados.

Al entrar a la carpeta **hooks** dentro de la carpeta **.git**, podemos encontrar el segundo error, el usuario **dev01** es dueño y amo de todo lo que hay dentro de esta carpeta, por lo que podemos utilizar el plugin **pre-commit** para poder escalar privilegios. 

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/18.png)

Pero ahora viene la pregunta del millón, ¿Qué es **pre-commit**? 

Pre-commit no es más que un plugin para **Git**, que ejecuta un script en **Bash** cada vez que se llame el comando **commit** de **Git**.

**Explotación**

La explotación en este caso es bastante simple, ya que nuestro usuario es el dueño de la carpeta **hooks** y podemos editar todo a nuestro gusto.

En un entorno normal donde se utilice el plugin ***pre-commit*,** dentro de la carpeta **hooks** deberíamos tener un script en **Bash** llamado ***pre-commit***, pero en este caso como no está el script, podemos copiar el archivo **pre-commit.sample** y renombrarlo **pre-commit**, para luego editarlo y tener una ejecución de comandos.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/19.png)



La edición es simple en este caso, solo abrimos el archivo **pre-commit** y agregamos una línea, siendo específicos la línea 10, que lo que hará será cambiarle los permisos a la **Bash** para que sea SUID, es decir, que se ejecutará como Root.

![](20.png)

Una vez hecho esto, solo debemos esperar hasta que Root ejecute ***/usr/local/bin/git-sync*** y para ejecutar la **Bash** y listo, habremos escalado privilegios.

![](/assets/img/posts/PrivEsc-Using-Git-Hooks/21.png)

**¿Por qué sucede esto?**

La razón por la que podemos escalar privilegios en este sistema, es debido a una mala gestión de privilegios en la carpeta **.git**, además, sucede que Root es el que ejecuta el **commit** de Git. La combinación de estos dos errores permite que sea posible escalar privilegios de manera súper sencilla en este sistema.

**¿Cómo evitarlo?**

Evitar este fallo es demasiado sencillo, lo más recomendable sería cambiar el usuario que ejecuta el commit, para que sea el usuario **dev01**. Otra recomendación sería cambiar los privilegios en la carpeta **.git**, para que Root sea dueño de todo y no pueda ser editado por otros usuarios.

Para concluir, quisiera recomendar a los usuarios de Git que verifiquen los permisos y privilegios de los archivos de Git en sus sistemas, para evitar que alguna persona malintencionada pueda escalar privilegios y gobernar el sistema de una manera tan sencilla.

**Referencias**

Git - Git Hooks. (s. f.). Git. <https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks>

Weaveworks. (s. f.). How to Start Left with Security Using Git Pre-Commit Hooks. <https://www.weave.works/blog/how-to-start-left-with-security-using-git-pre-commit-hooks>


