---
layout: post
title: "Esp32 Marauder DIY" 
author: snoody
feature-img: "/assets/img/posts/esp32-marauder-diy/banner.jpg"
thumbnail: "/assets/img/posts/esp32-marauder-diy/banner.jpg"
---

En este artículo, exploraremos cómo crear un Marauder utilizando un ESP32. En mi caso, utilicé un ESP32 de 38 pines con una extensión de antena para mejorar el rendimiento de la señal. 

![](/assets/img/posts/esp32-marauder-diy/1.png)

El Marauder es una herramienta versátil que permite llevar a cabo tanto ataques ofensivos como defensivos en redes WiFi y Bluetooth. Tiene similitudes con dispositivos como el Flipper Zero, que es popular por sus múltiples funcionalidades de pentesting. 

El proyecto del Marauder fue creado por JustCallMeKoko, quien ha abierto las puertas para que cualquier persona pueda montar su propio dispositivo a un bajo costo y con hardware accesible. 

**¿Qué es el ESP32 Marauder?** 

El Marauder como lo menciona en su github es una colección de herramientas que permiten interactuar con redes WiFi y dispositivos Bluetooth desde un ESP32. Estas herramientas permiten realizar pruebas de penetración, como escaneo de redes, desconexión de dispositivos, y captura de handshakes WiFi. También incluye herramientas para detectar y analizar dispositivos Bluetooth cercanos. 

##### **Materiales Utilizados** 

Los materiales aquí pueden varias dependiendo de tus objetivos, en mi caso yo quise hacerlo portátil, para poder usarlo en el momento que quiera. Pero se puede hacer sin problemas con una protoboard. 

* ESP32 
* Pantalla **Touch TFT LCD módulo ILI9341 2.4 Pulgadas** (Es recomendado usar 2.8 pulgadas) 

![](/assets/img/posts/esp32-marauder-diy/2.png)

* Jumpers

**Si deseas hacer la Versión Portátil:**

* TP4056

![3](/assets/img/posts/esp32-marauder-diy/3.png)

* Mini DC-DC boost module (No es necesario, pero es mejor prevenir algún corto) 

![4](/assets/img/posts/esp32-marauder-diy/4.png)

* Batería LiPo 3.7v, +1500mAh (recomendable, dependiendo de cuanto tiempo quieras que este encendido el Marauder) 

  ![5](/assets/img/posts/esp32-marauder-diy/5.png)

* Switch

![6](/assets/img/posts/esp32-marauder-diy/6.png)

* Modulo GPS (Opcional)  

  ![7](/assets/img/posts/esp32-marauder-diy/7.png)

  (Si alguien está interesado en hacer este proyecto me puede contactar para conseguirle los materiales baratos, discord: Snoody.)  

##### **Esquema de conexiones:** 

**Versión no portátil** 

![8](/assets/img/posts/esp32-marauder-diy/8.png)

**Versión Portátil** 

![9](/assets/img/posts/esp32-marauder-diy/9.png)

**Las conexiones son las mismas que la imagen de arriba)**



![10](/assets/img/posts/esp32-marauder-diy/10.png)

Disponible: https://oshwlab.com/epicnuevo123/marauder-version-38-pins

**Diseño de la placa PCB para versión portátil** 



**Parte Frontal**

![11](/assets/img/posts/esp32-marauder-diy/11.png)

**Parte Trasera**

![12](/assets/img/posts/esp32-marauder-diy/12.png)

*Si alguien esta interesado en tener una, me puede contactar para regalárselas* 

##### Flashear Esp32

Existen 2 formas de flashear el ESP32: 

1. **Vía web (Recomendada):** 

* Ventaja: Fácil de Flashear 

Nos dirigimos a el Web Uploader de Spacehuhn: https://esp.huhn.me  Nos aparecerá esto: 

![13](/assets/img/posts/esp32-marauder-diy/13.png)

Lo siguiente que haremos es Conectar nuestro ESP32 a el puerto USB de nuestra PC/LAPTOP, una vez conectado le damos a connect y se nos desplegara una barra donde nos mostrara nuestros puertos COM disponibles:

<img src="/assets/img/posts/esp32-marauder-diy/14.png" alt="14" style="zoom:67%;" />

Una vez seleccionado nuestro puerto del ESP32 nos aparecerá lo siguiente: 

![15](/assets/img/posts/esp32-marauder-diy/15.png)

Aquí nos dirigimos a el[ ](https://github.com/justcallmekoko/ESP32Marauder/wiki/update-firmware#using-spacehuhn-web-updater)[Github de ](https://github.com/justcallmekoko/ESP32Marauder/wiki/update-firmware#using-spacehuhn-web-updater)[*koko*](https://github.com/justcallmekoko/ESP32Marauder/wiki/update-firmware#using-spacehuhn-web-updater)[*:*](https://github.com/justcallmekoko/ESP32Marauder/wiki/update-firmware#using-spacehuhn-web-updater) 

Y en esta parte nos aparecerá los archivos .bin que usaremos para flashear nuestro ESP32

![16](/assets/img/posts/esp32-marauder-diy/16.png)

En Nuestro caso usaremos el ESP32 Marauderv4, v6, Kit, Mini 

Descargaremos lo siguiente: 

![17](/assets/img/posts/esp32-marauder-diy/17.png)

El Bootloader, partitions, Boot App. 

Por último, el firmware, al presionar el firmware nos redirigirá a esta página y la versión recomendada es la  v4 _old_hardware.bin 

<img src="C:\/assets/img/posts/esp32-marauder-diy/18.png" alt="18" style="zoom:80%;" />

**Posibles errores:** En mi caso intente instalar esta versión del firmware old hardware, pero esta no funciono, al momento de iniciar el marauder el *Touch* estaba invertido. 

En caso de que algún firmware te funcione mal, intenta con otro o mira este [enlace](https://github.com/justcallmekoko/ESP32Marauder/issues) 

El que me funciono a mi con pantalla de 2.4 pulgadas fue la v6_1.bin [Descarga](https://github.com/justcallmekoko/ESP32Marauder/releases/download/v1.0.0/esp32_marauder_v1_0_0_20240626_v6_1.bin) 

Una vez descargados los 4 archivos, regresamos a la pagina de Web Uploader y agregamos los archivos en el siguiente orden: 

 ![19](/assets/img/posts/esp32-marauder-diy/19.png)



Y le damos a **Program**, demora alrededor de 1-2 minutos (la imagen es para ilustrar, el botón)

![20](/assets/img/posts/esp32-marauder-diy/20.png)

Esta parte se puede hacer de primero para comprobar que se flashee bien, una vez flasheado nos podemos dirigir a esta [página](https://serial.huhn.me/)[ ](https://serial.huhn.me/)que es de SpaceHuhn también, esta es una terminal 

![21](/assets/img/posts/esp32-marauder-diy/21.png)

Si todo esta correctamente flasheado debería aparecer: 

*ESP32 MARAUDER JUSTCALLMEKOKO, ETC.*

2. **Vía Arduino IDE:**  



![22](/assets/img/posts/esp32-marauder-diy/22.png)

Desventaja: Mas vulnerable a tener problemas en compilación 

Recomiendo mirar este video si deseas instalarlo de esta forma: 

Aquí se encuentra paso a paso la instalación vía IDE: [Video](https://www.youtube.com/watch?v=3wcFsS64N3I&t=426s)[ ](https://www.youtube.com/watch?v=3wcFsS64N3I&t=426s)**Versión Final del ESP32 Portátil** 

##### Referencias: 

* Justcallmekoko. (s. f.). GitHub - justcallmekoko/ESP32Marauder: A suite of WiFi/Bluetooth offensive 	and 	defensive 	tools 	for 	the 	ESP32. 	GitHub. https://github.com/justcallmekoko/ESP32Marauder  

* Spacehuhn. (s. f.-a). ESPWebTool. https://esp.huhn.me/ 

* Spacehuhn. (s. f.). SpaceHuhn Serial Terminal. https://serial.huhn.me/  